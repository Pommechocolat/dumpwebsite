# coding: UTF-8
DOSSIER_SAUVE_LOCAL = "#{Dir.home}/Sites/_"

DOSSIER_SAUVEE_BD_REMOTE = "__dump__"

#------- Libs ---------------
require 'net/ftp'
require 'net/http'

require 'double_bag_ftps'

#---------------------------------
# Script PHP to dump database
#---------------------------------
PHP_DATABASE_DUMP = <<EOF_END
<?php
  mysql_connect($host, $user, $pass);
  mysql_select_db($base);
  mysql_query("SET NAMES UTF8");
  $tables = mysql_query("SHOW FULL TABLES WHERE `Table_type` =  'BASE TABLE';");
  //Pour chaque tables
  while ($donnees = mysql_fetch_array($tables)) {
    $table = $donnees[0];
    $res = mysql_query('SHOW CREATE TABLE '.$table);
    if ($res) {
      $insertions = 'DROP TABLE IF EXISTS '.$table.";\\n";
      $tableau = mysql_fetch_array($res);
      $tableau[1] .= ";\\n";
      $insertions .= $tableau[1];
      $req_table = mysql_query('SELECT * FROM '.$table);
      $nbr_champs = mysql_num_fields($req_table);
      //Pour chaque ligne
      while ($ligne = mysql_fetch_array($req_table)) {
        $insertions .= 'INSERT INTO '.$table.' VALUES(';
        //pour chanque champs
        for ($i=0; $i<=$nbr_champs-1; $i++) {
          $champ = $ligne[$i];
          if(!isset($champ)) {
            $insertions .= 'NULL';
          } else {
            $typeField = mysql_field_type($req_table, $i);
            $needQuote = ($typeField == "string" || $typeField == "blob" || $typeField == "datetime");
            if($needQuote) $insertions .= "'";
            $insertions .= mysql_real_escape_string($champ);
            if($needQuote) $insertions .= "'";            
          }
          $insertions .= ", ";
        } // Fin for champs
        $insertions = substr($insertions, 0, -2);
        $insertions .= ");\\n";
      } // Fin while lignes
      //echo $insertions;
      if ($insertions != '') {
        $f = @fopen($table.'.sql', 'w');
        $resultat= fwrite($f, $insertions);
        fclose($f);
      }
    } //fin if
  } //Fin tables
  $tables = mysql_query("SHOW FULL TABLES WHERE `Table_type` =  'VIEW';");
  //Pour chaque vues
  while ($donnees = mysql_fetch_array($tables)) {
    $table = $donnees[0];
    $res = mysql_query('SHOW CREATE VIEW '.$table);
    if ($res) {
      $insertions = 'DROP VIEW IF EXISTS '.$table.";\\n";
      $tableau = mysql_fetch_array($res);
      $vue = ereg_replace ( "CREATE.*VIEW" , "CREATE VIEW" , $tableau[1] );
      $insertions .= $vue.";\\n";  //Ajout un point-virgule suivi d'un saut à la ligne
    }
    if ($insertions != '') {
      $f = @fopen($table.'.sql', 'w');
      $resultat= fwrite($f, $insertions);
      fclose($f);
    }
  }
?>
EOF_END
#---------------------------------
# End Script PHP to dump database
#---------------------------------

#---------- classes -------------
class Entree_Dir
  attr_accessor :nom, :etat, :type, :taille
  def initialize(nom)
    @nom = nom
    @etat = 'supprime'
    @type = File::ftype(nom)
    @date = File::mtime(nom)
    @taille = File::size(nom)
  end
  
  def to_s
    "entrée locale : "+@nom+" - type: "+@type+" - date: "+@date.to_s+" - taille="+@taille.to_s+" - état: "+@etat
  end
end

class Entree_Ftp
  attr_accessor :nom, :etat, :type, :taille
  def initialize(entree)
    tableau = entree.split(' ')
    @nom = tableau[8..tableau.length].join(' ')
    @type = tableau[0][0]
    @date = tableau[5..7].join('-')
    @taille = tableau[4].to_i
    #@etat = 'supprime'
  end

  def to_s
    "entrée FTP : "+@nom+" - type: "+@type+" - date: "+@date.to_s+" - taille="+@taille.to_s #+" - état: "+@etat
  end

end
#--------- Fin classes ----------

#------- Fonctions ----------
def create_directory_if_not_exists(directory_name)
  Dir.mkdir(directory_name) unless File.exists?(directory_name)
end

def prepareLocaldump(urlSite)
  directory_name = DOSSIER_SAUVE_LOCAL
  create_directory_if_not_exists(directory_name)
  Dir.chdir(directory_name)
  directory_name += "/"+urlSite
  create_directory_if_not_exists(directory_name)
  Dir.chdir(directory_name)
end

def dumpFiles(ftp)
  #lit les fichiers présents dans le dossier
  hashLocalFile = Hash.new
  liste = Dir.entries('.')
  liste.each do |nomFile|
    case nomFile
      when  '.', '..'
      else
        entree = Entree_Dir::new(nomFile)
        hashLocalFile[nomFile] = entree
    end #case nomFile
  end #each liste
  #p "------- apres dir -> ftp"
  results = ftp.ls
  results.each do |entree|
    fich = Entree_Ftp::new(entree)
    case fich.nom
      when ".", ".."
      else
        traite = hashLocalFile[fich.nom]
        if fich.type == 'd' and (!traite or traite.type == 'directory')
          #cas d'un dossier
          create_directory_if_not_exists(fich.nom)
          Dir.chdir(fich.nom)
          ftp.chdir(fich.nom)
          #p Dir.pwd+" - "+ftp.pwd
          dumpFiles(ftp) # on appel la suite
          Dir.chdir('..')
          ftp.chdir('..')
        elsif fich.type == '-' and (!traite or traite.type == 'file')
          #cas d'un fichier
          if !traite or traite.taille != fich.taille
            begin
              ftp.get(fich.nom)
            rescue => msg
              #puts msg
              puts "chemin : "+ftp.pwd
              puts "LOG: #{fich.nom} can not be handled"
              #p fich
            end
          end #if traite
#        elsif fich.type == 'l'
#          #cas des liens symboliques
#          elements = fich.nom.split(" -> ")
#          nom = elements[0]
#          dest = elements[1]
#          #TODO Make sym link
#          p "Link à mettre en place "+fich.nom
#          #`ln -s #{dest} #{nom}`
#          Dir.chdir('..')          
        else
          p "Il y a eu un changment dans le type de l'entree : actuelle="+fich.to_s+" - précédent="+(traite ? traite.to_s : "pas dans en local")
        end #if typeFile
    end #case nomFile
  end #each results
end #def

def dumpBdd(ftp, urlSite, hostBdd, userBdd, passBdd, baseBdd, dossierWWW)
  ftp.chdir(dossierWWW)
  #Test présence dossier DOSSIER_SAUVEE_BD_REMOTE. Il faudra le supprimer à la fin
  existDumpDir = testPresenceDossierRemote(ftp, DOSSIER_SAUVEE_BD_REMOTE)
  if !existDumpDir
    ftp.mkdir(DOSSIER_SAUVEE_BD_REMOTE)
  end
  ftp.chdir(DOSSIER_SAUVEE_BD_REMOTE)
  #install le fichier de dump.php
  file = File.new("/tmp/"+DOSSIER_SAUVEE_BD_REMOTE+".php", "w")
  text = PHP_DATABASE_DUMP.force_encoding('utf-8')
  text = text.gsub("$host", "'"+hostBdd+"'")
  text = text.gsub("$user", "'"+userBdd+"'")
  text = text.gsub("$pass", "'"+passBdd+"'")
  text = text.gsub("$base", "'"+baseBdd+"'")
  file.write(text)
  file.close
  fichierPhp = "/tmp/"+DOSSIER_SAUVEE_BD_REMOTE+".php"
  ftp.put(fichierPhp)
  urlDumpDb = 'http://'+urlSite+"/"+DOSSIER_SAUVEE_BD_REMOTE+"/"+DOSSIER_SAUVEE_BD_REMOTE+".php"
  #Net::HTTP.get(URI(urlDumpDb))
  begin
    p Net::HTTP.get(urlSite, "/"+DOSSIER_SAUVEE_BD_REMOTE+"/"+DOSSIER_SAUVEE_BD_REMOTE+".php")
  rescue => e
    puts "oops ligne #{__LINE__}: #{e.message}"
    puts $!.inspect, $@  #http://fr.wikibooks.org/wiki/Programmation_Ruby/variables_globales_prédéfinies
  end
  ftp.chdir('..')
  ftp.chdir('..')
end

def testPresenceDossierRemote(ftpHandler, dossier)
  listeRacine = ftpHandler.ls
  existDumpDir = false
  listeRacine.each do |entree|
    entreeRacine = Entree_Ftp::new(entree)
    if entreeRacine.nom == dossier
      existDumpDir = true
      break
    end
  end
  existDumpDir
end

def clearRecursifDumpBddRemote(ftp, dossierWWW)
  ftp.chdir(dossierWWW)
  ftp.chdir(DOSSIER_SAUVEE_BD_REMOTE)
  results = ftp.ls
  results.each do |entree|
    tableau = entree.split
    nomFile = tableau[8..tableau.length].join
    case nomFile
      when ".", ".."
      else
        ftp.delete(nomFile)
    end
  end
  ftp.chdir('..')
  ftp.rmdir(DOSSIER_SAUVEE_BD_REMOTE)
end

def dumpAll(urlSite, userFtp, passFtp, hostFtp, userBdd, passBdd, baseBdd, hostBdd, dossierWWW)
  p "savegarde "+urlSite
  begin
    ftp = DoubleBagFTPS.open(hostFtp, userFtp, passFtp, nil, DoubleBagFTPS::EXPLICIT)
    ftp.passive = true
    p "pose le script de dump"
    dumpBdd(ftp, urlSite, hostBdd, userBdd, passBdd, baseBdd, dossierWWW)
    p "Dump Files"
    prepareLocaldump(urlSite)
    dumpFiles(ftp)
    p "Clear server"
    clearRecursifDumpBddRemote(ftp, dossierWWW)
    ftp.close
  rescue => e
    puts "oops ligne #{__LINE__}: #{e.message}"
    puts $!.inspect, $@  #http://fr.wikibooks.org/wiki/Programmation_Ruby/variables_globales_prédéfinies
  ensure
  end
  p "fin sauvegarde "+urlSite
end

def affichageMessageDump(urlSite, userFtp, passFtp, hostFtp, userBdd, passBdd, baseBdd, hostBdd, dossierWWW)
  message = "Exécution du dump avec les paramètres suivants :\n"
  message += "  URL site : #{urlSite}\n"
  message += "  user FTP : #{userFtp}\n"
  message += "  pass FTP : #{passFtp}\n"
  message += "  host FTP : #{hostFtp}\n"
  message += "  user BDD : #{userBdd}\n"
  message += "  pass BDD : #{passBdd}\n"
  message += "  nom  BDD : #{baseBdd}\n"
  message += "  host BDD : #{hostBdd}\n"
  message += "  dossier wwww : #{dossierWWW}\n"
  puts message
end

#----------- Programme -------------------------
if ARGV.length < 4 or ARGV.length > 9
  p 'Minimal Usage : ruby dumpwebsite.rb urlSite userFtp passFtp hostFtp '
  p 'Maximal Usage : ruby dumpwebsite.rb urlSite userFtp passFtp hostFtp userBdd passBdd base hostBdd dossierWWW'
else
  urlSite = ARGV[0]
  userFtp = ARGV[1]
  passFtp = ARGV[2]
  hostFtp = ARGV[3]
  userBdd = userFtp
  if ARGV[4]
    userBdd = ARGV[4]
  end
  passBdd = passFtp
  if ARGV[5]
    passBdd = ARGV[5]
  end
  baseBdd = userBdd
  if ARGV[6]
    baseBdd = ARGV[6]
  end
  hostBdd = 'localhost'
  if ARGV[7]
    hostBdd = ARGV[7]
  end
  dossierWWW = '.'
  if ARGV[8]
    dossierWWW = ARGV[8]
  end
  affichageMessageDump(urlSite, userFtp, passFtp, hostFtp, userBdd, passBdd, baseBdd, hostBdd, dossierWWW)
  dumpAll(urlSite, userFtp, passFtp, hostFtp, userBdd, passBdd, baseBdd, hostBdd, dossierWWW)
end


