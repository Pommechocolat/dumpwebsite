# Backup web Site
## Description
Script that dumps, with tfp connexion, all sources of a web site with all it's database tables. 

Create a backup of a site web with sources and database dump. It could be very easy to create a local version for dev, test, or demonstration with out internet connexion. I give explanation on how to configure a local serveur on mac with configuration to be in same configuration as you where on the remote server.

## How to use
* Minimal Usage : ruby dumpwebsite.rb urlSite userFtp passFtp hostFtp
* Maximal Usage : ruby dumpwebsite.rb urlSite userFtp passFtp hostFtp userBdd passBdd base hostBdd
